# ########################################################### #
# Pyxel detector simulation framework                         #
#                                                             #
# Example YAML configuration file                             #
# HxRG generic pipeline                                       #
# Author: Thibaut Prod'homme
# ########################################################### #


# yaml-language-server: $schema=https://esa.gitlab.io/pyxel/doc/latest/pyxel_schema.json
exposure:

  readout:
     times: numpy.logspace(-4, 0.2, 100)
     non_destructive: false

  outputs:
    output_folder: "output"
    save_data_to_file:      
      
ccd_detector:

  geometry:

    row: 256               # pixel
    col: 256               # pixel
    total_thickness: 40.    # um
    pixel_vert_size: 18.    # um
    pixel_horz_size: 18.    # um

  environment:
    temperature: 40        # K

  characteristics:
    quantum_efficiency: 1.                # -
    charge_to_volt_conversion: 5.93e-6      # V/e
    pre_amplification: 8.                   # V/V
    adc_bit_resolution: 16
    adc_voltage_range: [0.,4.096]
    full_well_capacity: 90000              # e

pipeline:
  # -> photon
  photon_collection:
        
    - name: illumination
      func: pyxel.models.photon_collection.illumination
      enabled: true
      arguments:
          level: 1000
          time_scale: 0.001
          
    - name: shot_noise
      func: pyxel.models.photon_collection.shot_noise
      enabled: true

  # photon -> charge
  charge_generation:
    - name: photoelectrons
      func: pyxel.models.charge_generation.simple_conversion
      enabled: true
        
    # - name: simple_dark_current
    #   func: pyxel.models.charge_generation.simple_dark_current
    #   enabled: true
    #   arguments:
    #     dark_rate: 20.0

  # charge -> pixel
  charge_collection:
    - name: simple_collection
      func: pyxel.models.charge_collection.simple_collection
      enabled: true

    - name: full_well
      func: pyxel.models.charge_collection.simple_full_well
      enabled: false
      
    - name: fixed_pattern_noise
      func: pyxel.models.charge_collection.fixed_pattern_noise
      enabled: true
      arguments:
        filename: "data/noise.npy"
        
  # pixel -> pixel
  charge_transfer:

  # pixel -> signal
  charge_measurement:
  
    - name: dc_offset
      func: pyxel.models.charge_measurement.dc_offset
      enabled: false
      arguments:
        offset: 1.
  
    - name: simple_measurement
      func: pyxel.models.charge_measurement.simple_measurement
      enabled: true
      
    - name: output_node_linearity_poly
      func: pyxel.models.charge_measurement.output_node_linearity_poly
      enabled: true
      arguments:
        coefficients: [0, 1, -0.5]  # e- [a,b,c] -> S = a + bx+ cx2 (x is signal in volts - non-amplified voltage!)
        
    - name: output_noise
      func: pyxel.models.charge_measurement.output_node_noise
      enabled: true
      arguments:
        std_deviation: 0.00002
    
        
  # signal -> image
  readout_electronics:
      
    - name: simple_amplifier
      func: pyxel.models.readout_electronics.simple_amplifier
      enabled: true
        
    - name: simple_adc
      func: pyxel.models.readout_electronics.simple_adc
      enabled: true
        
