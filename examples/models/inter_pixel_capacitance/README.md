# Single mode: inter-pixel capacitance

This notebook can be run in the cloud using Binder:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/esa%2Fpyxel-data/HEAD?urlpath=lab/tree/examples/models/inter-pixel%20capacitance/ipc.ipynb)

## References
 * Kannawadi, Arun & Shapiro, Charles & Mandelbaum, Rachel & Hirata, Christopher & Kruk, Jeffrey & Rhodes, Jason. (2016). "*The impact of interpixel capacitance in CMOS detectors on PSF shapes and implications for WFIRST*". Publications of the Astronomical Society of the Pacific. 128. 095001. [![doi](https://zenodo.org/badge/DOI/10.1088/1538-3873/128/967/095001.svg)](https://doi.org/10.1088/1538-3873/128/967/095001)
