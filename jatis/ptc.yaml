# ########################################################### #
# Pyxel detector simulation framework                         #
#                                                             #
# Example YAML configuration file                             #
# HxRG generic pipeline                                       #                         
# ########################################################### #


# yaml-language-server: $schema=https://esa.gitlab.io/pyxel/doc/latest/pyxel_schema.json
exposure:

  readout:
    times_from_file: "readout_times.txt"

  outputs:
    output_folder: "output"
    save_data_to_file:      
      
cmos_detector:

  geometry:

    row: 512              # pixel
    col: 512               # pixel
    total_thickness: 40.    # um
    pixel_vert_size: 15.    # um
    pixel_horz_size: 15.    # um

  environment:
    temperature: 80        # K

  characteristics:
    quantum_efficiency: 1.                # -
    charge_to_volt_conversion: 5.92e-6      # V/e
    pre_amplification: 6.                   # V/V
    adc_bit_resolution: 16
    adc_voltage_range: [0.,5.12]  # From 78.125uV/DN
    full_well_capacity: 91000              # e

pipeline:
  # -> photon
  photon_collection:
        
    - name: illumination
      func: pyxel.models.photon_collection.illumination
      enabled: true
      arguments:
          level: 630.982
          time_scale: 1.
          
    - name: shot_noise
      func: pyxel.models.photon_collection.shot_noise
      enabled: true

  # photon -> charge
  charge_generation:
    - name: photoelectrons
      func: pyxel.models.charge_generation.simple_conversion
      enabled: true

  # charge -> pixel
  charge_collection:
    - name: simple_collection
      func: pyxel.models.charge_collection.simple_collection
      enabled: true
        
    - name: simple_ipc
      func: pyxel.models.charge_collection.simple_ipc
      enabled: true
      arguments:
        coupling: 0.0074
        diagonal_coupling: 0.0006
        anisotropic_coupling: 0.001
        
    - name: simple_full_well
      func: pyxel.models.charge_collection.simple_full_well
      enabled: false

  # pixel -> pixel
  charge_transfer:

  # pixel -> signal
  charge_measurement:
  
    - name: simple_measurement
      func: pyxel.models.charge_measurement.simple_measurement
      enabled: true
    
    - name: physical_non_linearity
      func: pyxel.models.charge_measurement.physical_non_linearity
      enabled: true
      arguments:
        cutoff: 2.48
        n_acceptor: 1.e+18
        n_donor: 2.9e+15
        diode_diameter: 10.2
        v_bias: -0.25
        fixed_capacitance: 6.8e-15
        
  # signal -> image
  readout_electronics:
      
    - name: simple_amplifier
      func: pyxel.models.readout_electronics.simple_amplifier
      enabled: true
        
    - name: simple_adc
      func: pyxel.models.readout_electronics.simple_adc
      enabled: true
        
