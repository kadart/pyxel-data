{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "cde92e07-fc83-41c5-80b8-f5ef094207be",
   "metadata": {},
   "source": [
    "# [Data processing models:](https://esa.gitlab.io/pyxel/doc/latest/references/model_groups/data_processing_models.html) Observation mode\n",
    "\n",
    "##  Author\n",
    "2023&mdash;Constanze Seibert\n",
    "\n",
    "## Keywords\n",
    "data processing, dark current, observation mode\n",
    "\n",
    "## Learning Goals\n",
    "* use of [xarray.DataTree](https://xarray-datatree.readthedocs.io/en/latest/)\n",
    "* use of [Data processing models](https://esa.gitlab.io/pyxel/doc/latest/references/model_groups/data_processing_models.html) in observation mode\n",
    "\n",
    "## Prerequisites\n",
    "\n",
    "| Concepts | Importance | Notes |\n",
    "| -------- | ---------- | ----- |\n",
    "| {ref}`observation_mode`  | Necessary | Background |\n",
    "| [Xarray DataTree](https://xarray-datatree.readthedocs.io/en/latest/quick-overview.html) | Helpful | |\n",
    "| [Xarray](https://xarray.dev) | Helpful | |\n",
    "\n",
    "## Summary\n",
    "In the development towards Pyxel version 2.0, we introduced two new model groups. The **Data processing** model group at the end of the pipeline aims to do some Data processing inside of Pyxel.\n",
    "\n",
    "This notebook shows the usage of several basic operation models in the model group **Data processing**, namely ```statistics```, ```mean_variance```, ```linear_regression``` and ```signal_to_noise_ratio``` in [observation mode](https://esa.gitlab.io/pyxel/doc/latest/background/running_modes/observation_mode.html). \n",
    "We use Pyxel in observation mode by changing the dark rate for different exposure times."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5c555ad9-9222-4b09-b65d-29dfc2288415",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import packages\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import xarray as xr\n",
    "\n",
    "import pyxel"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c55bc9e4-9081-40b2-88c9-9b9fad42df24",
   "metadata": {},
   "outputs": [],
   "source": [
    "# load configuration file\n",
    "config = pyxel.load(\"data_example-obs.yaml\")\n",
    "\n",
    "observation = config.observation\n",
    "detector = config.detector\n",
    "pipeline = config.pipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14cf6f7b-a952-4467-82d5-5d4138d0cbea",
   "metadata": {},
   "source": [
    "Since version 1.8, Pyxel function ```run_mode()``` returns a [xarray.DataTree](https://xarray-datatree.readthedocs.io/en/latest/) and contains the output of the (Processed) Data from each model used in the YAML configuration file. \n",
    "\n",
    "\n",
    "### Result retrieved with ```run_mode()``` will show DataTree structure\n",
    "**Data group** contains the groups for each Data processing model used in the YAML configuration file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d06237cf-997c-484d-b2ec-8dd916f4dc7b",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "result = pyxel.run_mode(\n",
    "    mode=observation,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bca09f3e-6453-487a-97aa-60efb153a6ec",
   "metadata": {},
   "outputs": [],
   "source": [
    "xr.set_options(display_style=\"html\")  # text or html\n",
    "\n",
    "result"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9db11242-6349-437e-8de6-8807165d49e5",
   "metadata": {
    "tags": []
   },
   "source": [
    "Currently, we get the same output using the result retrieved from the ```run_mode()``` and the ```detector.data```. In a future Pyxel version the old usage will maybe deprecated.\n",
    "\n",
    "### Statistics model example\n",
    "\n",
    "Another way to access the **(Processed) Data** of a Data processing model is with \n",
    "```detector.data.<model_name>```, e.g. ```detector.data.statistics```. This contains the calculation of **variance, mean, min, max** and **count** for each data bucket *photon, pixel, signal* and *image* along the time. We can also access each bucket and calculation like a path of the DataTree."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "08217e51-8eb7-4ae2-927f-0c9e1e8d6c1f",
   "metadata": {},
   "outputs": [],
   "source": [
    "result[\"data/statistics/pixel/mean\"].plot(hue=\"dark_rate\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0ab90ea1-4dab-47d1-b19b-fbbbf048f5ed",
   "metadata": {},
   "source": [
    "### Signal-to-noise ratio example\n",
    "The model ```signal_to_noise_ratio``` computes the mean and the variance of the input array and calculates the ratio of that for each time step. \n",
    "\n",
    "We plot the SNR against the dark rate only for first 10 time steps, since the SNR converts towards zero with increasing exposure time and does not change significantly any further."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "acd4803c-1a28-45af-ac02-1a187fe0b0ef",
   "metadata": {},
   "outputs": [],
   "source": [
    "result[\"data/snr/signal/snr\"].isel(time=slice(0, 10)).plot(hue=\"time\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6af905b1-2d82-41ba-bc79-440dd11f2928",
   "metadata": {},
   "source": [
    "### Mean-variance model example\n",
    "The model ```mean_variance``` was developed to do a quick and easy Photon-Transfer curve (PTC) analysis, where you need the calculated mean and variance of the image. Computes a mean-variance 1D array that shows relationship between the mean signal of a detector and its variance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2ebceeca-8ec4-46c3-aee9-f4d20a41386f",
   "metadata": {},
   "outputs": [],
   "source": [
    "result[\"data/mean_variance/image/variance\"].plot.scatter(hue=\"dark_rate\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b3c9ee37-613e-4f91-b22b-a97cb7b03db1",
   "metadata": {},
   "source": [
    "### Linear regression model example\n",
    "The linear regression model was developed for quick and easy linear regression analysis of the simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5dbba56d-d548-4120-9cfb-9c8c3f913e1b",
   "metadata": {},
   "outputs": [],
   "source": [
    "result[\"/data/linear_regression\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c28a5999-2085-48d4-a5cc-1a31273d7298",
   "metadata": {},
   "outputs": [],
   "source": [
    "result[\"/data/linear_regression/image/\"].sel(x=0, y=0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6ea87c49-2949-44d2-b922-8dd88122f71c",
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset = xr.Dataset()\n",
    "dataset[\"slope\"] = result[\"/data/linear_regression/image/slope\"]\n",
    "dataset[\"intercept\"] = result[\"/data/linear_regression/image/intercept\"]\n",
    "dataset[\"image\"] = result[\"image\"]\n",
    "dataset[\"r2\"] = result[\"/data/linear_regression/image/r2\"]\n",
    "dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8146640b-7ace-4171-ae05-ee03fa85e4de",
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_linear_regression(x: np.ndarray, slope: float, intercept: float, r2: float):\n",
    "    x0 = x[0]\n",
    "    y0 = x0 * slope + intercept\n",
    "    plt.axline(\n",
    "        (x0, y0), slope=slope, color=\"r\", label=f\"linear fit:\\n{slope=:.1f}\\n{r2=:.5f}\"\n",
    "    )\n",
    "    ax = plt.gca()  # get current axis\n",
    "    ax.legend(frameon=True, fontsize=\"small\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6df4e592-748d-4bbb-8dae-5d9323c1abbc",
   "metadata": {},
   "source": [
    "The current model calculates the linear regression for the whole time period. In the future version we want to add a argument, where one can specify the range."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d41e5ebb-f260-4d93-b0b3-e0b62ce0c952",
   "metadata": {},
   "outputs": [],
   "source": [
    "xr.plot.FacetGrid(data=dataset.isel(x=0, y=0), col=\"dark_rate\", col_wrap=5).map(\n",
    "    plt.scatter, \"time\", \"image\", marker=\".\"\n",
    ").map(plot_linear_regression, \"time\", \"slope\", \"intercept\", \"r2\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "42b6cf73-096a-4e4d-978b-4ad45433225c",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
