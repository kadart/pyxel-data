# ########################################################### #
# Pyxel detector simulation framework                         #
#                                                             #
# Example YAML configuration file                             # 
# Created by Matej Arko                                       #
# ########################################################### #

# yaml-language-server: $schema=https://esa.gitlab.io/pyxel/doc/latest/pyxel_schema.json
exposure:

  readout:
    times: [4.]
    non_destructive:  true

  outputs:
    output_folder: "output"
    save_data_to_file:
      
apd_detector:

  geometry:

    row: 256               # pixel
    col: 320               # pixel

  environment:
    temperature: 80       # K

  characteristics:
    quantum_efficiency: 0.75            # -
    adc_voltage_range: [0., 2.5]
    adc_bit_resolution: 16
    avalanche_gain: 100
    pixel_reset_voltage: 6.
    roic_gain: 0.7962962962962962
    full_well_capacity: 100000

pipeline:
  # -> photon
  photon_collection:
    - name: load_image
      func: pyxel.models.photon_collection.load_image
      enabled: true
      arguments:
        image_file: data/default_fluxmap.npy
        align: "center"
  
  # photon -> charge
  charge_generation:
    - name: simple_conversion
      func: pyxel.models.charge_generation.simple_conversion
      enabled: true
    
    - name: apd_gain
      func: pyxel.models.charge_generation.apd_gain
      enabled: true

    - name: dark_current_saphira
      func: pyxel.models.charge_generation.dark_current_saphira
      enabled: true

  # charge -> pixel
  charge_collection:
    - name: simple_collection
      func: pyxel.models.charge_collection.simple_collection
      enabled: true
      
    - name: full_well
      func: pyxel.models.charge_collection.simple_full_well
      enabled: true

  # pixel -> signal
  charge_measurement:
  
    - name: output_pixel_reset_voltage_apd
      func: pyxel.models.charge_measurement.output_pixel_reset_voltage_apd
      enabled: true
      arguments:
        roic_drop: 3.3
        
    - name: ktc_noise
      func: pyxel.models.charge_measurement.ktc_noise
      enabled: true
        
    - name: simple_measurement
      func: pyxel.models.charge_measurement.simple_measurement
      enabled: true
      
    - name: readout_noise_saphira
      func: pyxel.models.charge_measurement.readout_noise_saphira
      enabled: true
      arguments:
        roic_readout_noise: 100.e-6
        controller_noise: 150.e-6
      
  # signal -> image
  readout_electronics:
  
    - name: simple_adc
      func: pyxel.models.readout_electronics.simple_adc
      enabled: true
