# ########################################################### #
# Pyxel detector simulation framework                         #
#                                                             #
# Example YAML configuration file for calibration mode        #
# Author: Bradley Kelman 2022                                 #
# ########################################################### #

# yaml-language-server: $schema=https://esa.gitlab.io/pyxel/doc/latest/pyxel_schema.json
calibration:
  result_type:          pixel
  
  result_fit_range:     [2065, 2300, 0, 1]
  target_fit_range:     [2065, 2300, 0, 1]
  result_input_arguments:
    - key: pipeline.charge_generation.load_charge.arguments.filename
      values:  ['profiles/input/r_G0_20.txt',
                'profiles/input/r_G0_30.txt',
                'profiles/input/r_G0_50.txt',
                'profiles/input/r_G0_60.txt',
                'profiles/input/r_G0_70.txt',
                'profiles/input/r_G0_80.txt',
                'profiles/input/r_G0_90.txt',
                'profiles/input/r_G0_100.txt',
                'profiles/input/r_G0_110.txt',
                'profiles/input/r_G0_120.txt']

  target_data_path: ['profiles/target/CTIr_G0_20.txt',
                     'profiles/target/CTIr_G0_30.txt',
                     'profiles/target/CTIr_G0_50.txt',
                     'profiles/target/CTIr_G0_60.txt',
                     'profiles/target/CTIr_G0_70.txt',
                     'profiles/target/CTIr_G0_80.txt',
                     'profiles/target/CTIr_G0_90.txt',
                     'profiles/target/CTIr_G0_100.txt',
                     'profiles/target/CTIr_G0_110.txt',
                     'profiles/target/CTIr_G0_120.txt']

  pygmo_seed:           60336
  num_islands:          40
  num_evolutions:       30
  num_best_decisions:   300
  topology: 'fully_connected'
  fitness_function:
    func:               pyxel.calibration.fitness.sum_of_squared_residuals
    arguments:
  algorithm:
    type:               sade
    generations:        30
    population_size:    200
    variant:            15

  parameters:
    - key:              pipeline.charge_transfer.cdm.arguments.trap_release_times
      values:           [_, _]
      logarithmic:      true
      boundaries:       [1.e-3, 1.e-1]
    - key:              pipeline.charge_transfer.cdm.arguments.trap_densities
      values:           [_, _]
      logarithmic:      true
      boundaries:       [1.e+2, 1.e+4]   

  outputs:
    output_folder:  '../../../Results/03_11_2022/'
    save_data_to_file:
      #-  detector_image_array: [fits]
      #- detector.image.array:   ['npy']
    save_calibration_data:
      - dataset: ['nc']
      - logs: ['csv','xlsx']



ccd_detector:

  geometry:

    row:   2300                  # pixel
    col:   1                  # pixel
    total_thickness: 40.        # um
    pixel_vert_size: 10.        # um
    pixel_horz_size: 10.        # um

  environment:
    temperature: 300        # K

  characteristics:
    quantum_efficiency: 1.                # -
    charge_to_volt_conversion: 1.       # V/e
    pre_amplification: 100                  # V/V
    adc_voltage_range: [0., 10.]
    adc_bit_resolution: 16
    full_well_capacity: 190000             # e

pipeline:

  # -> photon
  photon_collection:

  # photon -> charge
  charge_generation:
    - name: load_charge
      func: pyxel.models.charge_generation.load_charge
      enabled: true
      arguments:
        filename: 'G0/r_G0_130.txt'
        position: [0,0]            

  # charge -> pixel
  charge_collection:
    - name: simple_collection
      func: pyxel.models.charge_collection.simple_collection
      enabled: true
   

  # pixel -> pixel
  charge_transfer:
    - name: cdm
      func: pyxel.models.charge_transfer.cdm
      enabled: true
      arguments:
        direction: parallel
        trap_release_times: [0., 0.]      # calibrating this parameter
        trap_densities: [0., 0.]          # calibrating this parameter
        sigma: [1.e-10, 1.e-10]
        beta: 0.4
        max_electron_volume: 1.6200e-10   # cm^2
        transfer_period: 9.4722e-04       # s
        charge_injection: false

  # pixel -> signal
  charge_measurement:

  # signal -> image
  readout_electronics:

