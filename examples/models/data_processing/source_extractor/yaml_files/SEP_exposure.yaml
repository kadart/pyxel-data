# ########################################################### #
# Pyxel detector simulation framework                         #
#                                                             #
# Example YAML configuration file for exposure mode           #
# for the source extractor model 'extract_roi_to_xarray'.     #
# Author: Bradey Kelman 2023.                                 #
# ########################################################### #


exposure:

  outputs:
    output_folder:  'output'
    save_data_to_file:
    save_exposure_data:
      - dataset: ['nc']

ccd_detector:

  geometry:

    row: 2300               # pixel
    col: 2500              # pixel
    total_thickness: 40.    # um
    pixel_vert_size: 10.    # um
    pixel_horz_size: 10.    # um

  environment:
    temperature: 300        # K

  characteristics:
    quantum_efficiency: 1.                 # -
    charge_to_volt_conversion: 1       # V/e
    pre_amplification: 100                # V/V
    adc_voltage_range: [0., 10.]
    adc_bit_resolution: 16
    full_well_capacity: 190000               # e

pipeline:
  # photon -> photon
  photon_collection:
    - name: load_image
      func: pyxel.models.photon_collection.load_image
      enabled: true
      arguments:
        image_file: data/cti_free_v4_reversed_50e_bias.fits
        align: center
        convert_to_photons: false
        bit_resolution: 16

  # photon -> charge
  charge_generation:
    - name: photoelectrons
      func: pyxel.models.charge_generation.simple_conversion
      enabled: true


  # charge -> pixel
  charge_collection:
    - name: simple_collection
      func: pyxel.models.charge_collection.simple_collection
      enabled: true

  # pixel -> pixel
  charge_transfer:
    -  name: cdm
       func: pyxel.models.charge_transfer.cdm
       enabled: true
       arguments:
         direction: "parallel"
         trap_release_times: [0.3, 1.]
         trap_densities: [0.507, 0.715]
         sigma: [1.e-15, 1.e-15]
         beta: 0.3
         max_electron_volume: 1.e-10
         transfer_period: 1.e-4
         charge_injection: true  # only used for parallel mode
         
  # pixel -> signal
  charge_measurement:
    - name: simple_measurement
      func: pyxel.models.charge_measurement.simple_measurement
      enabled: true

  # signal -> image
  readout_electronics:
    - name: simple_adc
      func: pyxel.models.readout_electronics.simple_adc
      enabled: true
      
  data_processing:
    - name: extract_roi_to_xarray
      func: pyxel.models.data_processing.extract_roi_to_xarray
      arguments:
        thresh: 80
        minarea: 5
      enabled: true
      


