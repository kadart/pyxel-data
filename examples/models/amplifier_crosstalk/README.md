# Single mode: amplifier crosstalk

This notebook can be run in the cloud using Binder:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/esa%2Fpyxel-data/HEAD?urlpath=lab/tree/examples/models/amplifier%20crosstalk/crosstalk.ipynb)

## References
 * E. George et al.: *"Fast method of crosstalk characterization for HxRG detectors"*, J. Astron. Telesc. Instrum. Syst. 6(1), 011003 (2020). [![doi](https://zenodo.org/badge/DOI/10.1117/1.JATIS.6.1.011003.svg)](https://doi.org/10.1117/1.JATIS.6.1.011003)
